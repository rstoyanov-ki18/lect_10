#include <iostream>
using namespace std;

double pow(double a, int b) {
  
    double result = 1;

    if (b == 0) {
        return 1;
    }
    if (b < 0) {
        b = -b;
        for (long i = 0; i < b; i++) {
            result *= a;
        }

        return 1 / a;
    }
    else {
        result = pow(a * a, b / 2);
        return result;
    }
}

int main() {

    cout << pow(2.2, -2);
    return 0;
}